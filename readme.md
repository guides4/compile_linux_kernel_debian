# Compile Linux Kernel in Debian
Build a custom linux kernel from kernel.org for Debian

## Preparation

Get compressed file of linux kernel source code

```
wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.18.1.tar.xz
```

Get sign file of linux kernel tar file

```
wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.18.1.tar.sign

```

Extract the xz compressed file

```
unxz linux-5.18.1.tar.xz
```

Locate the keys for Greg Kroah-Hartman and Linus Torvalds

```
gpg --locate-keys torvalds@kernel.org gregkh@kernel.org
```

Verify the signature

```
gpg --verify linux-5.18.1.tar.sign
```

Get rid of "WARNING" message, trust the key using TOFU

```
gpg --tofu-policy good "Type RSA key"
```
```
gpg --trust-model tofu --verify linux-5.18.1.tar.sign
```

Extract the tar file

```
tar -xvf linux-5.18.1.tar
```

Go to the linux kernel source folder

```
cd linux-5.18.1
```

## Compile Linux Kernel

Ensure that the kernel tree is absolutely clean

```
make -j8 mrproper
```

Configure the linux kernel

```
make -j8 menuconfig
```

Build the kernel

```
make -j8
```

Build the modules

```
make -j8 modules
```

## Install Linux Kernel

Install the modules while as root

```
sudo make -j8 modules_install
```

For my case, this installs the modules in /lib/modules/5.18.1 <br/>

Copy linux image to /boot folder

```
sudo cp -v arch/x86/boot/bzImage /boot/vmlinuz-5.18.1
```

Copy config to /boot folder

```
sudo cp -v .config /boot/config-5.18.1
```

Copy the symbol table file to /boot folder (Optional)
```
sudo cp -v System.map /boot/System.map-5.18.1
```

Create Initramfs

```
sudo update-initramfs -c -k 5.18.1
```

These 3 file should be in the /boot folder:
* config-5.18.1
* initrd.img-5.18.1
* System.map-5.18.1 (Optional)
* vmlinuz-5.18.1

## Update Grub
Update the grub config file
```
sudo update-grub
```

## Install Nvidia Drivers

Install Nvidia drivers

```
sudo apt install nvidia-driver firmware-misc-nonfree
```

Install Nvidia kernel dkms for all linux kernel versions installed in the system

* Install Nvidia kernel dkms (Option 1)

```
sudo apt install nvidia-kernel-dkms
```

* Reinstall Nvidia kernel dkms (Option 2)

```
sudo apt install --reinstall nvidia-kernel-dkms
```

## Docker Issue Work Around

### Using kernel config to set Kernel Parameters before building the Linux kernel

**Method 1**:

Open the menu config
```
make -j8 menuconfig
```

Go to: Processor type and features --> Built-in kernel command line

Enable **Built-in kernel command line**
```
[*] Built-in kernel command line
```
Set the Kernel Parameter to **systemd.unified_cgroup_hierarchy=0**
```
(systemd.unified_cgroup_hierarchy=0) Built-in kernel command string
```

**Method 2**:

Enable it in the config file (.config)
Go the the root of the linux kernel source
```
cd linux-5.18.1
```

Edit the config file
```
vim .config
```

Set CONFIG_CMDLINE_BOOL to y and set CONFIG_CMDLINE to **systemd.unified_cgroup_hierarchy=0**
```
CONFIG_CMDLINE_BOOL=y
CONFIG_CMDLINE="systemd.unified_cgroup_hierarchy=0"
```

### Using Grub to set the Kernel Parameters

downgrade cgroup v2 to cgroup v1
```
sudo vim /etc/default/grub
```

* And the kernel parameter: **systemd.unified_cgroup_hierarchy=0**
```
GRUB_CMDLINE_LINUX="systemd.unified_cgroup_hierarchy=0"
```

## Resources

* https://www.kernel.org/signature.html#using-gnupg-to-verify-kernel-signatures
* https://manpages.debian.org/jessie/initramfs-tools/update-initramfs.8.en.html
* https://wiki.archlinux.org/title/Kernel/Traditional_compilation
* https://wiki.debian.org/NvidiaGraphicsDrivers#Debian_12_.22Bookworm.22
* https://github.com/opencontainers/runc/issues/2959
